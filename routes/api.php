<?php

use App\Http\Controllers\CineController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/peliculas/all', [CineController::class, 'getAll'])->name('peliculas.all');

Route::get('/pelicula/detail/{id}', [CineController::class, 'detail'])->name('pelicula.detail');

Route::post('/pelicula/create/', [CineController::class, 'create'])->name('pelicula.create');

Route::put('/pelicula/update/', [CineController::class, 'update'])->name('pelicula.update');

Route::post('/pelicula/upload/', [CineController::class, 'upload'])->name('pelicula.upload');

Route::delete('/pelicula/destroy/{id}', [CineController::class, 'destroy'])->name('pelicula.destroy');

