<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Movie;
use Illuminate\Support\Facades\Storage;

class CineController extends Controller
{

    public function create(Request $request)
    {
        $json = $request->input('json', null);
        $params = json_decode($json);
        $params_array = json_decode($json, true);

        if (!empty($params) && !empty($params_array)) {

            $params_array = array_map('trim', $params_array);

            $validate = validator($params_array, [
                'name'  => 'required|unique:m_pelicula',
                'duration' => 'required',
                'clasification'  => 'required|alpha'
            ]);

            if ($validate->fails()) {
                $data = array(
                    'status' => 'error',
                    'code'   =>  400,
                    'message' => 'La película no se pudo crear',
                    'errors' => $validate->errors()
                );
            } else {
                $image = $request->file('image');
               
                $url = NULL;
                if ($image) {

                    $validate = validator($request->all(), [
                        'image' => 'required|image|mimes:jpg,jpeg,png,gif',
                    ]);

                    if ($validate->fails()) {
                        
                        $data = array(
                            'status' => 'error',
                            'code'   =>  404,
                            'message' => 'Imagen incorrecta'
                        );
                        return response()->json($data, $data['code']);

                    } else {
                        $image = $request->file('image')->store('public/movies');
                        $url = Storage::url($image);
                    }
                }

                $movie = new Movie();
                $movie->name = $params_array['name'];
                $movie->poster = $url;
                $movie->duration = $params_array['duration'];
                $movie->clasification = $params_array['clasification'];

                $movie->save();

                $data = array(
                    'status' => 'succes',
                    'code'   =>  200,
                    'message' => 'La película se ha creado correctamente'
                );
            }
        } else {
            $data = array(
                'status' => 'error',
                'code'   =>  404,
                'message' => 'Los datos son incorrectos'
            );
        }
        return response()->json($data, $data['code']);
    }



    public function update(Request $request)
    {

        $json = $request->input('json', null);
        $params = json_decode($json);
        $params_array = json_decode($json, true);

        if (!empty($params) && !empty($params_array)) {

            $params_array = array_map('trim', $params_array);

            $validate = validator($params_array, [
                'id' => 'required',
                'name'  => 'unique:m_pelicula,name,' . $params_array['id'],
                'clasification'  => 'alpha'
            ]);

            if ($validate->fails()) {
                $data = array(
                    'status' => 'error',
                    'code'   =>  400,
                    'message' => 'La película no se pudo actualizar',
                    'errors' => $validate->errors()
                );
            } else {

                $movie_update = Movie::where('id',  $params_array['id'])->update($params_array);

                if (empty($movie_update)) {
                    $data = array(
                        'status' => 'error',
                        'code'   =>  404,
                        'message' => 'La película no existe'
                    );
                } else {
                    $data = array(
                        'status' => 'succes',
                        'code'   =>  200,
                        'message' => $movie_update
                    );
                }
            }
        } else {
            $data = array(
                'status' => 'error',
                'code'   =>  404,
                'message' => 'Los datos son incorrectos'
            );
        }
        return response()->json($data, $data['code']);
    }

    public function upload(Request $request)
    {

        $json = $request->input('json', null);
        $params = json_decode($json);
        $params_array = json_decode($json, true);

        if (!empty($params) && !empty($params_array)) {

            $id = $params_array['id'];
            $image = $request->file('image');


            $validate = validator($request->all(), [
                'image' => 'required|image|mimes:jpg,jpeg,png,gif',
            ]);

            $validateId = validator($params_array, [
                'id' => 'required',
            ]);


            if (!$image || $validate->fails() || $validateId->fails()) {

                $data = array(
                    'code' => 400,
                    'status' => 'error',
                    'message' => 'Error al subir Imagen'
                );
            } else {

                $movie = Movie::find($id);
                if (empty($movie)) {
                    $data = array(
                        'code' => 404,
                        'status' => 'error',
                        'message' => 'La película no existe'
                    );
                } else {

                    if ($movie->poster) {
                        
                        $url = str_replace('storage', 'public', $movie->poster);
                        Storage::delete($url);
                    }

                    $imagenes = $request->file('image')->store('public/movies');
                    $url = Storage::url($imagenes);
                    $movie->poster = $url;
                    $movie->update();

                    $data = array(
                        'code' => 200,
                        'status' => 'succes',
                        'message' => 'Imagen Subida'
                    );
                }
            }
        } else {
            $data = array(
                'status' => 'error',
                'code'   =>  404,
                'message' => 'Los datos son incorrectos'
            );
        }



        return response()->json($data, $data['code']);
    }

    public function detail($id)
    {
        $movie = Movie::find($id);

        if (!empty($movie)) {
            $data = array(
                'code' => 200,
                'status' => 'succes',
                'message' => $movie
            );
        } else {
            $data = array(
                'code' => 404,
                'status' => 'error',
                'message' => 'La película no existe'
            );
        }

        return response()->json($data, $data['code']);
    }

    public function getAll()
    {
        $movies = Movie::all();

        if (!empty($movies)) {
            $data = array(
                'code' => 200,
                'status' => 'succes',
                'message' => $movies
            );
        } else {

            $data = array(
                'code' => 404,
                'status' => 'error',
                'message' => 'No Existen Películas'
            );
        }
        return response()->json($data, $data['code']);
    }


    public function destroy($id, Request $request)
    {

        $movie = Movie::find($id);

        if (!empty($movie)) {
            $movie->delete();

            $url = str_replace('storage', 'public', $movie->poster);
            Storage::delete($url);

            $data = array(
                'code' => 200,
                'status' => 'succes',
                'message' => $movie
            );
        } else {
            $data = array(
                'code' => 404,
                'status' => 'error',
                'message' => 'La película no existe'
            );
        }

        return response()->json($data, $data['code']);
    }
}
